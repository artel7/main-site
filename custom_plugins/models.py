import os
from cms.models import CMSPlugin, Page
from cms.utils.compat.dj import python_2_unicode_compatible
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _

try:
    from cms.models import get_plugin_media_path
except ImportError:
    def get_plugin_media_path(instance, filename):
        """
        See cms.models.pluginmodel.get_plugin_media_path on django CMS 3.0.4+
        for information
        """
        return instance.get_media_path(filename)


class HideClass(models.Model):
    hide = models.BooleanField(_("hidden"), default=False, blank=True)

    class Meta:
        abstract = True


@python_2_unicode_compatible
class Intro(CMSPlugin, HideClass):
    """
    Plugin for about page.
    """
    major = models.CharField(_("major"), max_length=63,
                             help_text=_("Specifies text on top in tag <H2>."))
    image = models.ImageField(_("image"), upload_to=get_plugin_media_path,
                              blank=True)
    text = models.TextField(_("text"), blank=True,
                            help_text=_("Specifies main text with tags display"
                                        " filter '|safe'."))

    def __str__(self):
        return _(u"%s") % self.major


def technologies_logo_path(instance, filename):
    return 'logos/{}'.format(filename)


class Technology(models.Model):
    name = models.CharField(_('name'), max_length=255)
    logo = models.ImageField(
        _("logo"), upload_to=technologies_logo_path, blank=True)

    class Meta:
        verbose_name = "Technology"
        verbose_name_plural = "Technologies"

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Works(CMSPlugin, HideClass):
    """
    Plugin for wrok page.
    """
    major = models.CharField(_("major"), max_length=63,
                             help_text=_("Specifies text on top in tag <H2>."))
    total = models.CharField(_("total"), max_length=255, blank=True,
                             help_text=_("Specifies text after list of works in tag <p> style:italic."))

    def __str__(self):
        return _(u"%s") % self.major


@python_2_unicode_compatible
class Work(CMSPlugin, HideClass):
    """
    Plugin for each site definition.
    """
    title = models.CharField(_("title"), max_length=255, blank=True,)
    image = models.ImageField(_("image"), upload_to=get_plugin_media_path,
                              blank=True)
    link = models.CharField(_("link"), max_length=255, blank=True,
                            help_text=_("Specifies link where site is."))
    text = models.TextField(_("text"), blank=True,
                            help_text=_("Description for image"))
    technologies = models.ManyToManyField(Technology, blank=True)

    def __str__(self):
        return _(u"%s") % self.link


@python_2_unicode_compatible
class About(CMSPlugin, HideClass):
    """
    Plugin for wrok page.
    """
    major = models.CharField(_("major"), max_length=63,
                             help_text=_("Specifies text on top in tag <H2>."))
    description = models.TextField(_("description"), blank=True,
                                   help_text=_("Specifies text after major before mambers"))
    total = models.CharField(_("total"), max_length=255, blank=True,
                             help_text=_("Specifies text after list of members"))

    def __str__(self):
        return _(u"%s") % self.major


@python_2_unicode_compatible
class Member(CMSPlugin, HideClass):
    """
    Plugin for each site page.
    """
    image = models.ImageField(_("image"), upload_to=get_plugin_media_path,
                              blank=True)
    name = models.CharField(_("name"), max_length=255, blank=True,
                            help_text=_("Specifies link where site is."))
    text = models.TextField(_("text"), blank=True,
                            help_text=_("Description for image"))

    def __str__(self):
        return _(u"%s") % self.name


@python_2_unicode_compatible
class Contact(CMSPlugin, HideClass):
    """
    Plugin for each site page.
    """
    mail = models.CharField(_("mail"), max_length=63,
                            help_text=_("Specifies mail, which receive massage."))

    def __str__(self):
        return _(u"%s") % self.mail


class ContactModel(models.Model):
    """
    Model that collects massages.
    """
    mail = models.CharField(_("Address massage sent"), max_length=255)
    name = models.CharField(_("Name of subscriber"), max_length=64, blank=True)
    email = models.CharField(_("Callback address"), max_length=255, blank=True)
    message = models.TextField(_("Text in massage"))

    def __str__(self):
        return _(u"%s") % self.email


'''
@python_2_unicode_compatible
class Slide(CMSPlugin):
    """
    A Slide plugin that contains an image and some text.
    """

    image = models.ImageField(_("image"), upload_to=get_plugin_media_path)
    url = models.CharField(
        _("link"), max_length=255, blank=True, null=True,
        help_text=_("If present, clicking on image will take user to link."))

    page_link = models.ForeignKey(
        Page, verbose_name=_("page"), null=True,
        limit_choices_to={'publisher_is_draft': True}, blank=True,
        help_text=_("If present, clicking on image will take user to "
                    "specified page."))

    caption = models.TextField(
        _("caption"), max_length=255, blank=True, null=True,
        help_text=_("Specifies text that occurs on the slide."))

    def __str__(self):
        if self.caption:
            return self.caption[:40]
        elif self.image:
            # added if, because it raised attribute error when file wasn't
            # defined.
            try:
                return u"%s" % os.path.basename(self.image.name)
            except AttributeError:
                pass
        return u"<empty>"

    def clean(self):
        if self.url and self.page_link:
            raise ValidationError(
                _("You can enter a Link or a Page, but not both."))


@python_2_unicode_compatible
class Slider(CMSPlugin):
    """
    Plugin that can only contain Slides.
    """

    def __str__(self):
        return _(u"%s Images") % self.cmsplugin_set.all().count()
'''
