from django.contrib import admin

from custom_plugins.models import Intro, Works, Work, About, Member, Contact,\
                                 ContactModel, Technology

admin.site.register(Intro)
admin.site.register(Works)
admin.site.register(Work)
admin.site.register(About)
admin.site.register(Member)
admin.site.register(Contact)
admin.site.register(ContactModel)
admin.site.register(Technology)