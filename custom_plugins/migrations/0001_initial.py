# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import cms.models.pluginmodel


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
    ]

    operations = [
        migrations.CreateModel(
            name='Intro',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='custom_plugins_intro', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('major', models.CharField(help_text='Specifies text on top in tag <H2>.', max_length=63, verbose_name='major')),
                ('image', models.ImageField(upload_to=cms.models.pluginmodel.get_plugin_media_path, verbose_name='image', blank=True)),
                ('text', models.TextField(help_text="Specifies main text with tags display filter '|safe'.", verbose_name='text', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
