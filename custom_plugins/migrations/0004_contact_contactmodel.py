# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('custom_plugins', '0003_about_member'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='custom_plugins_contact', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('mail', models.CharField(help_text='Specifies mail, which receive massage.', max_length=63, verbose_name='mail')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='ContactModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mail', models.CharField(max_length=255, verbose_name='Address massage sent')),
                ('name', models.CharField(max_length=64, verbose_name='Name of subscriber', blank=True)),
                ('email', models.CharField(max_length=255, verbose_name='Callback address', blank=True)),
                ('message', models.TextField(verbose_name='Text in massage')),
            ],
        ),
    ]
