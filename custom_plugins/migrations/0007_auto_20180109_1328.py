# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import custom_plugins.models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_plugins', '0006_auto_20170513_1656'),
    ]

    operations = [
        migrations.CreateModel(
            name='Technology',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='name')),
                ('logo', models.ImageField(upload_to=custom_plugins.models.technologies_logo_path, verbose_name='logo', blank=True)),
            ],
            options={
                'verbose_name': 'Technology',
                'verbose_name_plural': 'Technologies',
            },
        ),
        migrations.AddField(
            model_name='work',
            name='technologies',
            field=models.ManyToManyField(to='custom_plugins.Technology'),
        ),
    ]
