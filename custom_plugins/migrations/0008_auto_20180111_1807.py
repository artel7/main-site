# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_plugins', '0007_auto_20180109_1328'),
    ]

    operations = [
        migrations.AddField(
            model_name='work',
            name='title',
            field=models.CharField(max_length=255, verbose_name='title', blank=True),
        ),
        migrations.AlterField(
            model_name='work',
            name='technologies',
            field=models.ManyToManyField(to='custom_plugins.Technology', blank=True),
        ),
    ]
