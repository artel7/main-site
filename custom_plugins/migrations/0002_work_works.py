# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import cms.models.pluginmodel


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('custom_plugins', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Work',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='custom_plugins_work', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('image', models.ImageField(upload_to=cms.models.pluginmodel.get_plugin_media_path, verbose_name='image', blank=True)),
                ('link', models.CharField(help_text='Specifies link where site is.', max_length=255, verbose_name='link', blank=True)),
                ('text', models.TextField(help_text='Description for image', verbose_name='text', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Works',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='custom_plugins_works', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('major', models.CharField(help_text='Specifies text on top in tag <H2>.', max_length=63, verbose_name='major')),
                ('total', models.CharField(help_text='Specifies text after list of works in tag <p> style:italic.', max_length=255, verbose_name='total', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
