# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_plugins', '0005_auto_20170513_1323'),
    ]

    operations = [
        migrations.AlterField(
            model_name='about',
            name='description',
            field=models.TextField(help_text='Specifies text after major before mambers', verbose_name='description', blank=True),
        ),
    ]
