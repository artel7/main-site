# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_plugins', '0004_contact_contactmodel'),
    ]

    operations = [
        migrations.AddField(
            model_name='about',
            name='hide',
            field=models.BooleanField(default=False, verbose_name='hidden'),
        ),
        migrations.AddField(
            model_name='contact',
            name='hide',
            field=models.BooleanField(default=False, verbose_name='hidden'),
        ),
        migrations.AddField(
            model_name='intro',
            name='hide',
            field=models.BooleanField(default=False, verbose_name='hidden'),
        ),
        migrations.AddField(
            model_name='member',
            name='hide',
            field=models.BooleanField(default=False, verbose_name='hidden'),
        ),
        migrations.AddField(
            model_name='work',
            name='hide',
            field=models.BooleanField(default=False, verbose_name='hidden'),
        ),
        migrations.AddField(
            model_name='works',
            name='hide',
            field=models.BooleanField(default=False, verbose_name='hidden'),
        ),
    ]
