# -*- coding: utf-8 -*-

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _
from .models import Intro, Works, Work, Member, About, Contact
from .forms import ContactForm
from django.http import HttpResponseRedirect, HttpResponse
from django.core.mail import send_mail
from django.conf import settings


class IntroPlugin(CMSPluginBase):
    model = Intro
    name = _('Intro')
    module = _("IntroPage")
    render_template = 'custom_plugins/intro.html'

plugin_pool.register_plugin(IntroPlugin)


class WorkPlugin(CMSPluginBase):
    model = Work
    name = _('Work')
    module = _("WorksPage")
    render_template = 'custom_plugins/work-detail.html'

plugin_pool.register_plugin(WorkPlugin)


class WorksPlugin(CMSPluginBase):
    model = Works
    name = _('Works')
    module = _("WorksPage")
    render_template = 'custom_plugins/works.html'
    allow_children = True
    child_classes = ["WorkPlugin"]

    def render(self, context, instance, placeholder):
        context.update({
            'instance': instance,
            'placeholder': placeholder,
        })
        return context
    
plugin_pool.register_plugin(WorksPlugin)


class MemberPlugin(CMSPluginBase):
    model = Member
    name = _('Member')
    module = _("AboutPage")
    render_template = 'custom_plugins/member.html'

plugin_pool.register_plugin(MemberPlugin)


class AboutPlugin(CMSPluginBase):
    model = About
    name = _('About')
    module = _("AboutPage")
    render_template = 'custom_plugins/about.html'
    allow_children = True
    child_classes = ["MemberPlugin"]

    def render(self, context, instance, placeholder):
        context.update({
            'instance': instance,
            'placeholder': placeholder,
        })
        return context
    
plugin_pool.register_plugin(AboutPlugin)


class ContactPlugin(CMSPluginBase):
    model = Contact
    name = _('Contact')
    module = _("ContactPage")
    render_template = 'custom_plugins/contact.html'

    def render(self, context, instance, placeholder):
        request = context['request']
        form = ContactForm(request.POST) if request.method == 'POST'\
                                         else ContactForm()
        if form.is_valid():
            form.save()
            message = u'{}\n\r{}\n\r{}'.format(request.POST['email'],
                                               request.POST['name'],
                                               request.POST['message'])
            send_mail(
                'Hello Artel7 Team',
                message,
                settings.EMAIL_HOST_USER,
                [instance.mail],
                fail_silently=False,
            )
            return HttpResponseRedirect('/')
        context.update({
           'instance': instance,
        })
        return context

plugin_pool.register_plugin(ContactPlugin)
