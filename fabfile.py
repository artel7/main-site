from fabric.api import *
from contextlib import contextmanager
from StringIO import StringIO

def artel():
    env.build = '/home/fabric/'
    env.virtualenv = '/home/fabric/.venv'
    env.activate = 'source %(virtualenv)s/bin/activate' % env
    env.code_dir = '%(build)s' % env
    env.hosts = ['178.63.196.1']
    env.user = 'root'
    env.password = 'cerf,kznm'
    # env.warn_only = True
    # env.
artel()

def test_1():
    fh = StringIO();
    with cd(env.code_dir): 
        run("git status", stdout=fh)
        modified_files = run('git log -n 1 --pretty="short" --name-only')
        puts('///////////////////////////')
        # for line in fh.readlines():
            # puts(line)

@contextmanager
def virtualenv():
    with cd(env.virtualenv), prefix(env.activate), cd(env.code_dir):
        yield


def start():
    with cd('/home'): 
        run('git clone https://rborodinov@bitbucket.org/rborodinov/fabric.git')
    with cd(env.code_dir): 
        run('virtualenv .venv')
    with virtualenv():
        run('pip install -r requirements.txt')
        run('python manage.py migrate')
        run('python manage.py collectstatic --noinput --link')
        # abort('Breackpoint for tests')


'''
        with settings(# hide('warnings', 'running', 'stdout', 'stderr'),
                      warn_only=True
                      ):
'''
def update():
    with virtualenv():
        run('git pull')
        modified_files = run('git log -n 1 --pretty="short" --name-only')
        if modified_files.find('requirements.txt') !=-1:
            puts('Need to update requirements packages')
            run('pip install -r requirements.txt')
        run('python manage.py migrate')
        run('python manage.py collectstatic --noinput --link')
        # abort('Breackpoint for tests')
